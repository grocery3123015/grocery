import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:glocery_app/api/api_service.dart';
import 'package:glocery_app/models/category.dart';
import 'package:glocery_app/models/pagination.dart';
import 'package:glocery_app/models/product.dart';
import 'package:glocery_app/models/product_filter.dart';

final categoriesProvider =
    FutureProvider.family<List<Category>?, PaginationModel>(
  (ref, paginationModel) {
    final apiRepository = ref.watch(apiService);

    return apiRepository.getCategories(
      paginationModel.page,
      paginationModel.pageSize,
    );
  },
);
final homeProductProvider =
    FutureProvider.family<List<Product>?, ProductFilterModel>(
        (ref, productFilterModel) {
  final apiRespository = ref.watch(apiService);

  return apiRespository.getProducts(productFilterModel);
});
