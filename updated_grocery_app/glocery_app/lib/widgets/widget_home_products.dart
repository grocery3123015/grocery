import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:glocery_app/components/product_card.dart';
// import 'package:glocery_app/models/category.dart';
import 'package:glocery_app/models/pagination.dart';
import 'package:glocery_app/models/product.dart';
import 'package:glocery_app/models/product_filter.dart';
import 'package:glocery_app/providers.dart';

class HomeProductsWidget extends ConsumerWidget {
  const HomeProductsWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    // List<Product> list = List<Product>.empty(growable: true);

    // list.add(
    //   Product(
    //       productName: "Britania Choco chill cake",
    //       category: Category(
    //           categoryName: "Backery & Buiscuits",
    //           categoryImage:
    //               "/uploads/categories/1712776284485-bakeryand Buiscuits.jpeg",
    //           categoryId: "6616e45c1f8ee817e1a659a7"),
    //       productShortDescription:
    //           "Britania chocolate cake has soft and delicious",
    //       productPrice: 50,
    //       productSalePrice: 45,
    //       productImage: "/uploads/products/1712776556509-imggg.png",
    //       productSKU: "GA-123",
    //       productType: "simple",
    //       stockStatus: "IN",
    //       productId: "6616e56c1f8ee817e1a659ad"),
    // );
    // list.add(
    //   Product(
    //       productName: "Britania Choco chill cake",
    //       category: Category(
    //           categoryName: "Backery & Buiscuits",
    //           categoryImage:
    //               "/uploads/categories/1712776284485-bakeryand Buiscuits.jpeg",
    //           categoryId: "6616e45c1f8ee817e1a659a7"),
    //       productShortDescription:
    //           "Britania chocolate cake has soft and delicious",
    //       productPrice: 50,
    //       productSalePrice: 45,
    //       productImage: "/uploads/products/1712776556509-imggg.png",
    //       productSKU: "GA-123",
    //       productType: "simple",
    //       stockStatus: "IN",
    //       productId: "6616e56c1f8ee817e1a659ad"
    //       ),
    // );
   
    return Container(
      color: const Color(0xffF4F7FA),
      child: Column(
        children: [
          const Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,

            children: [
              Padding(
                padding: EdgeInsets.only(left: 16, top: 15),
                child: Text(
                  "Top 10 Products",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              )
            ], // Text
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: _productsList(ref),
                        // child: _buildProductList(list),

          )
        ],
      ),
    );
  }

  Widget _productsList(WidgetRef ref) {
    final product = ref.watch(
      homeProductProvider(
        ProductFilterModel(
          paginationModel: PaginationModel(page: 1, pageSize: 10),
        ),
      ),
    );

    return product.when(
      data: (list) {
        return _buildProductList(list!);
      },
      error: (_, __) {
        return const Center(child: Text("ERROR"));
      },
       
      loading: () => const Center(child: CircularProgressIndicator()),
    );
  }

  Widget _buildProductList(List<Product> products) {
    return Container(
      height: 200,

      alignment: Alignment.centerLeft,

      child: ListView.builder(
        physics: const ClampingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemCount: products.length,
        itemBuilder: (context, index) {
          var data = products[index];

          return GestureDetector(
            onTap: () {},

            child: ProductCard(
              model: data,
            ), 
          );
        },
      ), 
    ); 
  }
}
