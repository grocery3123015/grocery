class Config {
  static const String appName = "Grocery App";
  static const String apiURL = "localhost:4000";
  static const String imageURL = "http://localhost:4000";

  static const String categoryAPI = "api/category";
  static const String productAPI = "api/product";

  static const int pageSize = 10;
  static const String  currency  = "₹";
}
