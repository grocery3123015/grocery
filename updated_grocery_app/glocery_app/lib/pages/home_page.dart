import 'package:flutter/material.dart';
import 'package:glocery_app/components/product_card.dart';
import 'package:glocery_app/models/category.dart';
import 'package:glocery_app/widgets/widget_home_categories.dart';
import 'package:glocery_app/widgets/widget_home_products.dart';

import '../models/product.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: ListView(
          children: [
            HomeCategoriesWidget(),  
            HomeProductsWidget() 
         // ProductCard(model:model
          //)
          ],
        ),
      ),
    );
  }
}
